import '../wigets/cat.dart';
import "package:flutter/material.dart";
import 'dart:math' show pi;

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> with TickerProviderStateMixin {
  late Animation<double> catAnimation;
  late AnimationController catController;
  // Flap
  late AnimationController flapController;
  late Animation<double> flapAnimation;

  @override
  void initState() {
    super.initState();
    catController = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );
    catAnimation = Tween(begin: -50.0, end: -90.0)
        .animate(CurvedAnimation(parent: catController, curve: Curves.easeIn));

    flapController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );
    flapAnimation = Tween(begin: pi * .6, end: pi * .65)
        .animate(CurvedAnimation(parent: flapController, curve: Curves.easeIn));
    flapAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        flapController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        flapController.forward();
      }
    });
    flapController.forward();
  }

  onTap() {
    if (catController.status == AnimationStatus.completed) {
      catController.reverse();
      flapController.forward();
    } else if (catController.status == AnimationStatus.dismissed) {
      catController.forward();
      flapController.stop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Flutter App"),
        ),
        body: GestureDetector(
          child: Center(
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                buildCatAnimation(),
                buildBox(),
                buildLeftFlap(),
                buildRightFlap(),
              ],
            ),
          ),
          onTap: onTap,
        ),
      ),
    );
  }

  Widget buildCatAnimation() {
    return AnimatedBuilder(
      animation: catAnimation,
      builder: (BuildContext context, child) {
        return Positioned(
          child: child as Widget,
          top: catAnimation.value,
          right: 0.0,
          left: 0.0,
        );
      },
      child: const Cat(),
    );
  }

  Widget buildBox() {
    return Container(
      height: 200.0,
      width: 200.0,
      color: Colors.brown,
    );
  }

  Widget buildLeftFlap() {
    return Positioned(
        left: 3.0,
        child: AnimatedBuilder(
            animation: flapAnimation,
            builder: (BuildContext context, Widget? child) {
              return Transform.rotate(
                  angle: flapAnimation.value,
                  alignment: Alignment.topLeft,
                  child: child as Widget);
            },
            child: Container(
              height: 10.0,
              width: 125.0,
              color: Colors.brown,
            )));
  }

  Widget buildRightFlap() {
    return Positioned(
        right: 3.0,
        child: AnimatedBuilder(
            animation: flapAnimation,
            builder: (BuildContext context, Widget? child) {
              return Transform.rotate(
                  angle: -flapAnimation.value,
                  alignment: Alignment.topRight,
                  child: child as Widget);
            },
            child: Container(
              height: 10.0,
              width: 125.0,
              color: Colors.brown,
            )));
  }
}
