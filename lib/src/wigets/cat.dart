import 'package:flutter/material.dart';

class Cat extends StatelessWidget {
  const Cat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      'https://ik.imagekit.io/mwbsya52ton/2282_SkVNQSBGQU1PIDEwMTYtMzY_qvE0xXDDU.png',
      scale: 0.8,
    );
  }
}
